from django.http import HttpResponse
from django.middleware.csrf import get_token
from . import models

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Django YouTube (version 2)</h1>
    <h2>Deseleccionables</h2>
      <ul>
      {selected}
      </ul>
    <h2>Seleccionables</h2>
      <ul>
      {selectable}
      </ul>
  </body>
</html>
"""

VIDEO = """
      <li>
        <form action='/' method='post'>
          <a href='{link}'>{title}</a>
          <input type='hidden' name='id' value='{id}'>
          <input type='hidden' name='csrfmiddlewaretoken' value='{token}'>
          <input type='hidden' name='{name}' value='True'>
          <input type='submit' value='{action}'>
        </form>
      </li>
"""


def build_html(name, selected, action, token):

    html = ""
    videos = models.Video.objects.filter(selected=selected)  # Guarda los videos filtrando por el campo seleecioando
    for video in videos:
        html = html + VIDEO.format(link=video.link,
                                   title=video.title,
                                   id=video.id,
                                   name=name,
                                   action=action,
                                   token=token)
        # Pasa los parámetros necesarios para crear según el formato de VIDEO
    return html


def change_video(id, selected):

    video = models.Video.objects.get(id=id)  # Coge el video con el que coincida el id
    video.selected = selected                  # Cambia el estado del booleano seleccionado
    video.save()                             # Guarda los cambios


def index(request):

    if request.method == 'POST':        # Si la petición es POST
        if 'id' in request.POST:        # Si el campo 'id' del formulario no está vacío
            if request.POST.get('Seleccionar'):  # Si el valor del campo action = Seleccionar
                change_video(id=request.POST['id'], selected=True)  # Pasar a la función, el id y selected
            elif request.POST.get('Deseleccionar'):  # Si el valor del campo action = Deseleccionar
                change_video(id=request.POST['id'], selected=False)

    # Si el método no es POST
    csrf_token = get_token(request)

    # Si están seleccionados
    seleccionados = build_html(name='Deseleccionar', selected=True, action='Deseleccionar', token=csrf_token)

    # Si son seleccionables
    seleccionables = build_html(name='Seleccionar', selected=False, action='Seleccionar', token=csrf_token)
    htmlBody = PAGE.format(selected=seleccionados,  selectable=seleccionables)
    return HttpResponse(htmlBody)
